# JavaScript Guide
---

## Index
* 1
	* A. 변수
	* B. 함수정의
	* C. 스코프
	* D. 기본자료형
	* E. 비교연산자
	* F. typeof와 instanceof
	* G. Template Literal

* 2
	* A. Arguments
	* B. ...args
	* C. Callback 패턴
	* D. Promise 패턴
	* E. Closure(클로저)
	* F. this
	* G. call, apply

* 3
	* Module
	* async, await
	* 
	* ...
	* 
	* 메모리 관리
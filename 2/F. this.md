# 2-F. this
---
JS에서의 `this` 객체 자신을 가리키는 것 이외에 `function` 내에서도 사용할 수 있다.

`function`이 객체인것을 감안하면 결국 `this`가 객체 안에서 사용된다고 봐도 무방하다.

[strict mode](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions_and_function_scope/Strict_mode)와 non-strict mode에 따라 동작의 차이가 있다.

---

## 전역 스코프
전역 스코프에서 `this`는 stict mode이던 아니던 상관 없이 전역 객체를 가리킨다.

```
console.log(this.document === document); // true

// 웹 브라우저에서 전역 객체는 window 객체이다.
console.log(this === window); // true

this.a = 37;
console.log(window.a); // 37
```

---

## 함수 스코프
함수 내부에서 `this`는 함수 호출 방식에 따라 가리키는 객체가 달라진다.

### 단순 함수 호출
```
function f1(){
  return this;
}

f1() === window; // global object
```

이 경우 `f1`은 전역함수로 취급 되기 때문에 `this`는 `f1`을 품은 전역 객체(브라우저에서는 `window`)를 가리킨다.

하지만, 이 동작은 조금 이상하다고 생각할 수 있다.

우리는 앞서 `function`은 객체라고 배웠다. `f1`은 그 자체로 객체이므로 `this`는 `f1`을 나타내야 하는 것 아닐까?

일단은 `'use strict'` 코드를 `f1`에 추가하여 다시 보도록 하자

```
function f1(){
  'use strict';

  return this;
}

f1() === window; // false
```

`'use strict'`를 추가하게 되면 `this`는 더이상 `window`가 아니게 된다.

*strict mode* 에서는 `this`는 실행 컨텍스트 부분에서 정의가 되어야 한다.

> *strict mode*는 JS 문법의 엄격함을 잡기 위해 나왔다.

> `function` 내부나, 전역 스코프에 `'use strict'` 라고 코드를 추가해 주면 *strict mode*로 동작한다.

> *strict mode*에서는 엄격한 문법(예: 정의되지 않은 변수를 호출)이 적용 된다.

즉, `f1`을 호출 할 때 `this`를 정의해 줘야 한다는 것이다.

`this`는 원래 객체 자신을 가리키는 것이므로 `f1`의 `this`를 지정하는 것은 객체를 가리키도록 호출하면 된다.

```

window.f1() === window; // true

```

위의 코드처럼 `window` 객체에서 호출 한다는 것을 명시해 주면 된다.

혹은 아래와 같은 방법도 있다.

```

f1.apply(this) === window; // true
f1.call(this) === window; // true

```

`apply`와 `call`에 대해선 다른 파트에 다루도록 하겠다.


### 객체의 메소드에서
```
var o = {
  prop: 37,
  f: function() {
    return this.prop;
  }
};

console.log(o.f()); // logs 37
```

객체의 메소드에서는 우리가 알고있는 그 `this`처럼 작용한다. 이건 *strict mode*와는 관계가 없다.
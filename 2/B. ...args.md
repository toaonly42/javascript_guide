#2-B. ...args
---

## *...args*
`...args`는 ES6에서 추가 됐다. `Arguments`의 불편함을 개선하여 나왔다.  
`...args`라고 쓰긴 했지만, 사실은 `...` 이 추가 된것이다.  
`...a`라고 쓰든 `...b`라고 쓰든 `...abc` 라고 쓰든 상관이 없다.  
...은 연산자로 취급되며, `function`에 정의되지 않은 모든 파라미터를 가지고 있는 배열객체를 반환해준다.
---

### 2-A에서 사용한 함수를 개량해보자.

```

function min(num1, num2, ...args)	{
	var result = num1;

	if(num1 > num2)	result = num2;

	if(arguments.length === 2)	{
		return result;
	} else	{
		args.forEach(function(num)	{
			if(result > num)	result = num;
		});

		return result;
	}
};

console.log(min(1, 2));
console.log(min(1, 2, 3));
console.log(min(1, 2, 3, -1));

```

... 연산자의 반환값은 유사 배열(like array)이 아닌 진짜 배열이다. 따라서 `Array`에 있는 모든 메소드를 사용할 수 있다.
단, ES6에서 추가된 연산자이기 때문에 IE8,9에서는 사용할 수 없으니 *babel*을 사용하여 코드를 변환하도록 하자.  

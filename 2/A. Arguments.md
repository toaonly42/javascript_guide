#2-A. Arguments
---

## *Arguments*
JS는 언어 특성상 `function`을 굉장히 많이 사용한다.  
JS는 시작부터 끝까지 `function`만을 사용하여 작성이 가능할 만큼 `function`에 대한 의존도가 굉장히 높다.  
따라서 다른 언어에 비해 `function`에 관한 메소드가 굉장히 많고 특이한 기능도 많다.  

`Arguments`는 그 특이한 기능 중 하나이다.
---

### function에서 정의하지 않은 parameter를 사용하고 싶어
`Arguments`는 이 바람에 아주 딱 맞는 친구다. 코드로 바로 넘어가 보자.  

```

function min(num1, num2/*, num3, ..., numN */)	{
	if(arguments.length === 2)	{
		if(num1 > num2)	return num2;
		else	return num1;
	} else	{
		var result = num1;

		for(var index = 1; index < arguments.length; index++)	{
			var num = arguments[index];

			if(result > num)	result = num;
		}

		return result;
	}
};

console.log(min(1, 2));
console.log(min(1, 2, 3));
console.log(min(1, 2, 3, -1));

```

`arguments`는 유사 배열(like array) 객체이다. `length`를 알 수 있고, 그 `length`로 반복문을 돌릴 수 있다.  
또한 배열처럼 `index`를 가지고 있기 때문에 배열처럼 값을 가지고 오는 것이 가능하다.  
단, 배열과 비슷한 것이지 배열이 아니기 때문에 JS의 `Array` 객체에 있는 메소드를 사용할 수 없다.
일반적인 상황에서는 사용할 일이 거의 없고, Core 라이브러리를 만들 경우 여러 상황에서 파라미터를 입력 받을 때 자주 사용한다.
#2-C. Callback 패턴
---

## Callback 패턴
Callback 패턴은 하나의 API를 호출하여 그 반환값(return value)을 `function`으로 받아 처리하는 패턴을 말한다.  
JS에서는 비동기 처리가 굉장히 많은데, 이 처리를 유용하게 해주는 패턴이 바로 Callback 패턴이다.  
---

### Callback 패턴의 작성
작성은 굉장히 쉽다.

```
function each(arr, callback)	{
	for(var index = 0; index < arr.length; index++)	{
		callback(arr[index], arr);
	}
}

each([ 1, 2, 3, 4 ], function(value)	{
	console.log(value);
});

each([ 1, 2, 3, 4 ], function(value)	{
	console.log(value * value);
});

each([ 1, 2, 3, 4 ], function(value)	{
	console.log(value * 2);
});
```
위에는 비동기 방식은 아니지만, callback 패턴의 강점을 잘 보여주는 코드이다.  
each라는 함수를 하나 만들고 callback으로 각각 _원래의 값_, _제곱값_, _x2 값_을 콘솔로그에 찍어주고 있다.  
callback 패턴의 강점은 본래의 API의 기능은 그대로 두고 그 결과에 대한 처리 방식이 다양할 경우 유용하게 쓸 수 있는 패턴이다. 
---

### ajax에서의 Callback 패턴
JS에서는 *ajax*를 이용한 비동기 통신을 굉장히 많이 처리하게 된다.  
아래는 *jQuery* 기준의 소스이다.

```
function getHeroes(callback)	{
	$.ajax({
		url: '/heroes',
		method: 'GET',
		success: function(res)	{
			callback(res.heroes);
		},
		error: function()	{
			console.error('failed get heroes. :(');
		}
	});	
};

getHeroes(function(heroes)	{
	console.log(heroes);
});

```
비동기 통신의 결과값은 언제오는지 정확히 알 수 없기 때문에, callback으로 처리하는 것이 편하다.
#2-D. Promise 패턴
---

## Promise(계약이행) 패턴
Promise 패턴은 Callback 패턴의 단점을 보완하고자 나온 패턴이다.  
구조는 조금 복잡한데, 아래와 같은 절차를 따른다.  
>               (지킴)
				Fulfilled
	Pending ->            -> Settled
    (대기)		Rejected     (완료)
                (어김)

대기 상태는 아직 Promise를 실행하기 전 상태이다.  
Fulfilled, Rejected는 Promise가 실행된 이 후의 결과이다. 약속을 지켰거나 어겼거나 둘 중 하나의 상태로 빠지게 된다.  
지키든 어기든 결론이 나면 Settled로 빠지게 된다.

---

### Callback 패턴의 단점?
Callback 패턴의 단점은 Callback Hell이라 불리는 소스 코드 최악의 가독성을 말한다.  
아래 코드를 보도록 하자.

```
function getUser(id, callback)	{
	$.ajax({
		url: '/user/' + id,
		success: callback
	});
};

function updateUser(user, callback)	{
	$.ajax({
		url: '/user/' + user.id,
		method: 'POST',
		data: user,
		success: callback
	});
};

function renderUser(user)	{
	Object.keys(user).forEach((key) => {
		$('#' + key).text(user[key]);
	});
};

getUser('userid', function(user)	{
	renderUser(user);

	user.point++;

	updateUser(user, function()	{
		getUser(user, function(user)	{
			renderUser(user);
		});
	});
});

```
위에는 비동기 처리가 두 번(getUser, updateUser)이 들어가 callback의 깊이가. 세 번 까지 들어간 구조이다.  
세 번까지도 코드가 조금 지저분한데, 이 구조가 네 번, 다섯 번 들어가게 되면 코드의 가독성이 극악으로 떨어지게 된다.
---

### Callback Hell에서 벗어나자
Promise패턴을 사용하면 Callback Hell에서 벗어날 수 있다.
ES6에서는 Promise 객체가 추가되어 개발자가 특별히 Promise 패턴을 구현하기 위한 코드작성을 할 필요가 없다.  
위의 코드를 Promise 객체를 이용해 개량해보자.

```
function getUser(id)	{
	return new Promise(function(resolve, reject)	{
		$.ajax({
			url: '/user/' + id,
			success: resolve,
			error: reject
		});
	});
};

function updateUser(user)	{
	return new Promise(function(resolve, reject)	{
		$.ajax({
			url: '/user/' + user.id,
			method: 'POST',
			data: user,
			success: resolve,
			error: reject
		});
	}
};

function renderUser(user)	{
	Object.keys(user).forEach((key) => {
		$('#' + key).text(user[key]);
	});
};

getUser('userid')
	.then(user => {
		renderUser(user);
		user.point++;

		return user;
	})
	.then(updateUser)
	.then(user => user.id)
	.then(getUser)
	.then(renderUser)
	.catch(error => console.error(error));

```
then은 약속을 지켰을때(Fulfilled)의 callback이고 catch는 약속을 어겼을 때(Rejected)의 callback을 호출 해준다.  
소스코드를 보면 알겠지만 가독성이 굉장히 좋다는 것을 알 수 있다.


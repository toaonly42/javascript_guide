# Closure
- - - -

## Closure?
Closure는 함수내에 생명주기를 가지는 함수를 정의하는 일종의 디자인 패턴이다.

우리는 보통 함수 안에서 지역변수(Local Variable)를 선언한다. 그리고 *Closure*는 지역함수(Local Function) 생각하면 된다.

지역함수로 선언하면 되므로 코드는 아래와 같다.

```
function init()	{
	var name = 'Mozillar';
	
	function displayName()	{
		alert(name);
	}
	
	displayName();
}

init();
```

`init()`안에는 지역변수 `name`과 지역함수 `displayName()`을 선언하였다.  `displayName()`은 `init()`안에서만 사용할 수 있다.

- - - -

## 도대체 이걸 왜 쓰는 거냐?
이걸 왜 쓰냐하면, JS는 `function`을 다른 프로그래밍 언어에서 취급하는 것과는 다르게 취급하기 때문이다.

**1-D. 기본자료형**에서 다루었듯이, JS의 모든 객체의 원형은 `Object`이다. 그리고 `function` 역시 JS에서 객체로 취급하며 당연히 객체이므로 `Object`를 상속 받은 객체이다.

`function`을 객체라고 생각하면 이제 단순해진다. 그래서 "뭐가 단순한거야?" 라고 묻는 다면 위에서 다룬 예제 코드를 이렇게 바꿔 보겠다.

```
function init()	{
	var name = 'Mozillar';
	
	var displayName = function()	{
		alert(name);
	}
	
	displayName();
}

init();
```

이번에는 `displayName`이라는 변수에 `function`을 대입했다. 함수를 변수에 대입하는 것이 가능하고, 이 말은 변수처럼 사용이 가능하다는 뜻이다.

그리고 그 함수를 반환하는 것 역시 가능하다는 말이 된다.

```
function makeAdder(x)	{
	return function(y)	{
		return x + y;
	}
}

var add5 = makeAdder(5),
	  add10 = makeAdder(10);

console.log(add5(2));
console.log(add10(2));
```

지금은 `makeAdder`라는 함수를 선언하고, `add5`와 `add10`에 각각 파라미터로 `5`와 `10`을 담아 호출하여 대입했다.

`makrAdder`를 호출해도 `function(y) { return x + y; }`가 반환되기 때문에 `add5`와 `add10`은 함수로서 취급된다.

각각 파라미터로 `5`와 `10`을 담았기 때문에 `x`는 그 값들이 대입된 상태다.

`add5`은 `add10` 이 후에도 계속 호출 할 때마다 파라미터 `y`를 넣어줘야 하지만, `x`는 더 이상 변하지 않으므로 계속해서 각각 `5`와 `10`으로 `y`를 더하는 함수를 호출할 수 있게 된다.

`makeAdder`는 결과적으로 보면 함수를 생성하는 함수가 된다. 이를 **함수 팩토리** 라고 한다.

### 뭔가 어려운데?
어렵다고 느낀다면 그 이유는 기존에 우리가 알고 있던 함수가 아니기 때문일 것이다.
정확히는 익숙하지 않다고 해야할 것 이다. JS에서는 `function`은 객체로서 취급한다고 생각하면 조금 더 받아들이기 쉽다.

- - - -
## Closure로 JS에서의 클래스 *private* 구현하기
ES5 까지는 `class`키워드가 제공되지 않았기 때문에 `class`를 구현할 때 `function`을 가지고 구현하였다.

```
var Car = function Car(name, color) {
	this.name = name;
	this.color = color;
}

var car1 = new Car('Car1', 'Red');

console.log(car1.name);
console.log(car1.color);
```
이렇게 객체를 구현 했다.

그렇지만 JS는 `private`와 `public` 키워드가 없으며 객체의 변수, 메소드는 `public`으로 정의된다. 정상적인 방법으로는 `private`을 지원해 주지 않는다.

`private`을 정의하기 위해서 **Closure** 패턴을 도입하게 됐다.

```
var Car = function Car() {
	var _name, _color;

	function Car(name, color) {
		this.getName = function() { return _name; }
		this.setName = function(name) { _name = name; }

		this.getColor = function() { return _color; }
		this.setColor = function(name) { _color = color; }
	}

	return Car;
}

var car1 = new Car('Car1', 'Red');

console.log(car1.name); // ReferenceError
console.log(car1.color);	// ReferenceError

car1.setName('Car2');
car1.setColor('Blue');

console.log(car1.getName());
console.log(car1.getColor());

```

위의 소스는 `Car` 객체에 있는 `name`과 `color` 변수를 `private` 으로 구현한 소스이다.

`Car` 함수의 반환값은 그 안에 있는 `Car` 함수가 된다. 반환값이 내부의 `Car`여도 같은 레벨에 있는 `_name`과 `_color`는 이미 할당이 된 변수이다

이 반환된 함수는 `this`객체로 `name`과 `color`가 정의되어 있지 않다. 하지만, 각각 `getName`, `setName`, `getColor`, `setColor`로 `_name`과 `_color`를 정의하고 가져올 수 있다.
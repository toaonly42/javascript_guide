# 1-A. 변수
---

## *var*
JS에서 변수는 값을 담을 수 있는 저장 공간을 의미한다.

기존 JS에서는 변수 선언시 `var` 키워드를 반드시 필요로 하지 않았다.

하지만, 현재 JS에서는 `var` 키워드를 쓰는 것을 필히 권고 하고 있다.

```
var a, b;
```

변수를 담는 것은 대입 연산자를 통해 가능하다.

```
var message = 'Hello JavaScript!';
```

`var`를 붙여 다시 재정의 하는 것이 가능하다.

```
var message = 'Hello JavaScript!';

console.log(message);	// Hello JavaScript!

var message = 'This is JavaScript';

console.log(message);	// This is JavaScript
```
---

### *let*
`let`은 Scope(스코프) 블록 범위에서 Life Cycle(생명주기)를 갖는 변수를 선언할 때 사용한다.

ECMAScript6(ECMAScript 2015, 이하 ES6)에서 추가된 JS의 새로운 예약어이다.

```
{
	let a = 10;

	console.log(a);	// 10
}
```

`var`와는 다르게 `let` 키워드를 사용하여 다시 재정의 하면 `SyntaxError`가 발생한다.

다시 재정의 할 경우에는 변수명만을 사용하여 정의하면 된다.

```
{
	let a = 10;

	console.log(a);	// 10

	a = 20;

	conosle.log(a);	// 20

	let a = 30;	// SyntaxError
}
```

`let`은 당신이 본래 알고 있는 지역변수(Local Variable) 개념을 잘 구현해 놨다.

`var`는 아니라고 묻는다면 아래 소스를 통해서 확연히 할 수 있다.

```
function printArrayValue(arr)	{
	for(var index = 0; index < arr.length; index++)	{
		var value = arr[index];

		console.log(value);
	}

	console.log('Last index: %d, Last Value: %s', index - 1, value);
};

printArrayValue([ 1, 2, 3, 4 ]);
```

```
function printArrayValue(arr)	{
	for(let index = 0; index < arr.length; index++)	{
		let value = arr[index];

		console.log(value);
	}

	console.log('Last index: %d, Last Value: %s', index - 1, value);	// ReferenceError
};

printArrayValue([ 1, 2, 3, 4 ]);
```

`let`은 블록 스코프 내에서만 생명주기를 갖기 때문에 `for`의 블록을 나온 순간 `index`와 `value`는 사용할 수 없게 된다.

## *const*
`const`는 상수 값을 정의할 때 사용한다. 키워드의 의미 그대로 재정의시 TypeError가 발생하므로 유의하자.

```
const PI = 3.14;	// 반드시 초기값을 정의 해줘야 한다.

console.log(PI);	// 3.14
```

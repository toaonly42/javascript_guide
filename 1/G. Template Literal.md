# 1-G. Template Literal
---
Template Literal(템플릿 리터럴)은 ES6에서 새롭게 추가된 문자열 정의 방식이다.
(이번 내용은 MDN에 있는 내용을 번역하여 가져옴)

---

## 문법(Syntax)
> \`string text\`
>
> \`string text line 1
>  string text line2\`
>
> \`string text ${expression} string text\`
> 
> tag \`string text ${expression} string text\`
---

## 설명(Description)
템플릿 리터럴은 따옴표(')와 쌍따옴표(") 대신에 back-tick(\` \`)([grave accent](http://en.wikipedia.org/wiki/Grave_accent))을 사용한다.

placeholder를 사용할 수 있다. placeholder의 사용은 달러 기호와 중괄호를 사용한다.(${표현식})

placeholder안에서의 표현식자리에는 일반적인 JS 코드가 들어갈 수 있다.

### 다중 개행 문자열
기존 방식에서는 개행 문자(\n)를 사용해야만 했다.

```
console.log('string text line 1\n' +
'string text line 2');
```

템플릿 리터럴을 사용하여 같은 결과물을 다음과 같이 코드로 짤 수 있다.

```
console.log(`string text line 1
string text line 2`);
```

### 표현식 삽입
기존 방식에서는 문자열 사이에 표현식을 넣으려면 다음과 같이 코드를 짜야 했다.

```
var a = 5;
var b = 10;
console.log('Fifteen is ' + (a + b) + ' and\nnot ' + (2 * a + b) + '.');
```

템플릿 리터럴을 사용하면 더욱 가독성 좋게 JS문법을 문자열 내부에 녹여내서 사용할 수 있다.
```
var a = 5;
var b = 10;
console.log(`Fifteen is ${a + b} and
not ${2 * a + b}.`);
```

### Tagged 템플릿
더 발전된 템플릿 리터럴은 바로 *tagged* 템플릿이다. Tags는 템플릿 리터럴이 함수를 거쳐 변환된 것을 말한다.
tag 함수의 첫 번째 인자는 문자열 값을 배열로 가지고 있다. 나머지 인자는 표현식이다.

```
var person = 'Mike';
var age = 28;

function myTag(strings, personExp, ageExp) {

  var str0 = strings[0]; // "that "
  var str1 = strings[1]; // " is a "

  // There is technically a string after
  // the final expression (in our example),
  // but it is empty (""), so disregard.
  // var str2 = strings[2];

  var ageStr;
  if (ageExp > 99){
    ageStr = 'centenarian';
  } else {
    ageStr = 'youngster';
  }

  return str0 + personExp + str1 + ageStr;

}

var output = myTag`that ${ person } is a ${ age }`;

console.log(output);
```

### Raw 문자열
tagged 템플릿의 첫 번째 인자는 `raw`라는 특별한 프로퍼티를 가지고 있다.
[escape sequences](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types#Using_special_characters_in_strings) 과정을 거칠필요 없이 표현식을 제외한 문자열을 배열 형태로 가져와 사용할 수 있다.

```
function tag(strings) {
  console.log(strings.raw[0]);
}

tag`string text line 1 \n string text line 2`;
```

# 1-C. 스코프(Scope)
---

## 스코프(Scope)란?
변수나 함수가 영향을 미치는 범위를 말한다.
변수의 Life Cycle(생명주기) 범위라고 생각하면 된다.

```
function foo()	{
	var b = 1;		// 변수 b는 foo 함수 내부에서만 영향을 미친다.

	console.log(b);
};

console.log(b);	// ReferenceError
```
---

## *if*와 *for*에서 응용
`if`와 `for`에서 활용할 수 있다.

```
var arr = [ 1, 2, 3, 4 ];

for(var index = 0; index < arr.length; index++)	{
	console.log(arr[index]);
}

console.log(index);	// 4
```

```
var a;

if((a = 1) === 1)	{
	alert(true);
} else	{
	alert(false);
}

console.log(a);	// 1
```
---

## 주의사항
되도록이면 같은 범위에서는 동일한 명칭의 변수나 함수 정의를 삼가하는 것이 좋다.

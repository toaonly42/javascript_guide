# 1-F. typeof와 instanceof
---

## typeof
`typeof`는 자료형을 반환하는 연산자이다.  
사용법은 간단하다.  

> typeof __value__

```

// 아래의 값들을 확인해보자.
console.log(typeof(1));
console.log(typeof 1);
console.log(typeof []);
console.log(typeof {});
console.log(typeof new Date);
console.log(typeof function()	{});
console.log(typeof Symbol());
console.log(typeof undefined);
console.log(typeof null);
console.log(typeof true);

```
---

### instanceof
`instanceof`는 생성자(`constructor`)의 `prototype`을 비교해주는 연산자이다.  
> __object__ instanceof __constructor__
  
__object__ 가 __constructor__.prototype의 존재여부를 판별하여 참, 거짓의 결과값을 반환해준다.

```
// 아래의 값을 확인해보자.
console.log(1 instanceof Number);
console.log(new Number(1) instanceof Number);
console.log('0' instanceof String);
console.log(new String('0') instanceof String);
console.log(true instanceof Boolean);
console.log(new Boolean(true) instanceof Boolean);
console.log(function()	{} instanceof Function);
console.log({ a: 1 } instanceof Object);
console.log([ 1, 2, 3, 4 ] instanceof Array);
console.log([ 5, 6, 7, 8 ] instanceof Object);
console.log(new Date instanceof Date);
console.log(new Date instanceof Object);
```

특이한점이 눈에 띄는데, `1 instanceof Number` 와 `new Number(1) instanceof Number`의 결과값이 다르다는 것을 알 수 있다.
왜 다르냐면 `1`은 단순히 값(value)이고 `new Number(1)`은 인스턴스화된 객체(object)이기 때문이다.  
즉, 값에 대해서는 `prototype`을 찾을 수 없다.  

그런데, 우리는 앞서 이런 코드를 봤다.

```
var foo = 1;

console.log(foo.constructor === Number);
```

이게 JS에서의 이상한 부분이다. 값의 `constructor`를 비교하면 `true`라고 나오지만, `instanceof`를 사용하면 `false`라고 나온다.  
아래 코드를 실행해보자.

```
console.log(1);
console.log(new Number(1));
```

실행해보면 위의 `1`은 단순히 `1`이 찍힌다.  

그리고 `new Number(1)`은
>
	Number {1}
	__proto__: Number
		constructor: ƒ Number()
		toExponential: ƒ toExponential()
		toFixed: ƒ toFixed()
		toLocaleString: ƒ toLocaleString()
		toPrecision: ƒ toPrecision()
		toString: ƒ toString()
		valueOf: ƒ valueOf()
		__proto__: Object
		[[PrimitiveValue]]: 0
	[[PrimitiveValue]]: 1
이렇게 보여진다.
`instanceof`는 비교할 값의 __proto__ 를 비교하여 같은지를 판단하기 때문에 단순한 값은 `instanceof`로 확인할 수 없다.  

그렇다면 단순한 값은 자료형을 어떻게 비교할까?  
앞서 소개했던 `typeof`를 사용하거나 `constructor`를 사용하면된다.

```
console.log(typeof 1 === 'number');
console.log((1).constructor === Number);
```

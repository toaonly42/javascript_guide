# 1-D. 기본자료형
---

## 기본자료형
JS에서는 최소한 알고 시작해야할 정말 기본자료형이 있다.

바로 `String`, `Number`, `Boolean`, `null`, `undefined`, `Object`, `Symbol` 이다.

추가로 `Function`과 `Array`도 같이 살펴보자.
---

### *String*
`String`은 문자열 객체다. JS에서 단일문자와 문자열은 모두 `String` 객체에 속하게 된다.
변수에 문자열(혹은 단일문자)을 대입하면 자동으로 `String` 객체로 할당된다.

```
var str = 'Hello JavaScript!';

// 변수의 객체원형이 무엇인지 확인해보자.
console.log(str.constructor);
```
---

### *Number*
`Number`는 실수 객체다. `integer`, `long`, `float`, `double`을 가리지 않고 `Number`에 속하게 된다.  
변수에 실수값을 대입하면 자동으로 `Number` 객체로 할당된다.

```
var num = 100,
	PI = 3.14;

// 변수의 객체원형이 무엇인지 확인해보자.
console.log(num.constructor);
console.log(PI.constructor);
```
---

### *Boolean*
`Boolean`은 논리연산에서 참, 거짓만을 가진 객체다. 값은 `true`와 `false` 두 가지 뿐이다.

```
var result1 = 2 > 1,
	result2 = 3 === 2;

// 변수의 객체원형이 무엇인지 확인해보자.
console.log(result1.constructor);
console.log(result2.constructor);
```
---

### *null*
JS에서는 특이하게도 `null`이 별도의 객체로 존재한다.  

```
var foo = null,
	bar;

// 아래의 값들을 확인해보자.
console.log(foo);
console.log(bar);
```
---

### *undefined*
JS에서는 "값이 정의 되지않음"에 대한 객체가 별도로 존재한다.  
그 객체가 바로 `undefined`이다.

```
var foo;

// 아래의 값을 확인해보자.
console.log(foo);

// ReferenceError
console.log(bar);
```
---

### *Object*
`Object`는 원형 객체이다. 원형 객체라고 하면 이해하기 힘들다.  
이해하기 쉽게 *JSON*을 생각하면 된다.  
변수에 *JSON* 타입의 값을 대입하면 `Object` 객체로 할당된다.

```
var obj = { a: 1234 };

// 변수의 객체원형이 무엇인지 확인해보자.
console.log(obj.constructor);
```
---

### *Symbol*
`Symbol`은 ES6와서 추가된 객체다.  
자세한 설명은 **Symbol**편에서 진행하겠다.  
---

### *Array*
`Array`는 배열 객체다. 배열의 기본 자료형은 정해져있지 않다.  
JS의 기본 객체는 아니지만, 자주 사용하기 때문에 이번 단원에서 간략히 소개한다.

```
var numArr = [ 1, 2, 3, 4 ],
	strArr = [ 'ABC', 'DEF', 'G', 'HIJK' ],
	arr = [ 1, 2, 'A', 'B' ];


// 변수의 객체원형이 무엇인지 확인해보자.
console.log(numArr.constructor);
console.log(strArr.constructor);
console.log(arr.constructor);
```
---

### *Function*
`Function`은 함수 객체이다. JS는 특이하게도 함수 객체가 별도로 존재한다.  
일반적으로 `function` 키워드가 붙어서 정의되면 `Function` 객체로 할당된다.  
JS의 기본 객체는 아니지만, 자주 사용하기 때문에 이번 단원에서 간략히 소개한다.

```
function foo()	{};

// 변수의 객체원형이 무엇인지 확인해보자.
console.log(foo.constructor);
```
---

### *proto*
JS에는 모든 객체의 원형이 되는 `prototype` 객체가 존재하는데 그것이 바로 `Object` 이다.

JS에서는 엄격한 의미에서의 *완전히 독립된 객체 정의*가 불가능하다.  
왜냐하면 JS에 있는 모든 객체는 Object라는 빈 객체의 *Prototype*(프로토타입)이기 때문이다.  

아래는 MDN에서 설명하고 있는 내용이다.

> JavaScript에서 모든 객체는 Object의 후손입니다. 즉 모든 객체는 `Object.prototype`으로부터 메소드 및 속성을 상속합니다, 비록 재정의(override)될 수 있지만 (null 프로토타입인 `Object` 제외, 가령 `Object.create(null)`). 예를 들어, 다른 생성자의 프로토타입은 `constructor` 속성을 재정의하고 자체 `toString()` 메소드를 제공합니다. `Object` 프로토타입 객체의 변화는 프로토타입 체인을 따라 좀 더 재정의되는 그러한 변화의 대상인 속성 및 메소드를 제외하고 모든 객체로 전해집니다.

위의 설명을 요약은 아래와 같다.

> **JS의 모든 객체는 `Object`의 후손이다.**

즉, 당신이 JS에서 어떤 객체를 만들어도 `Object`의 상속을 피할 수 없다는 것을 의미한다.  
`proto` 는 당신이 정의한 객체의 부모 객체의 `prototype`을 가리키는 객체를 의미한다.

*객체의 상속*편에서 자세히 다루겠지만, JS에서의 상속은 다른 객체지향 언어와는 그 방식이 아주 특이하다.  
물론 ES6에 와서는 형식 자체는 다른 언어들과는 많이 유사해 졌지만, 그렇다고 내부 구조가 바뀐것은 아니다.  

우선은 `proto` 와 `prototype`이 있다는 것만 알아두고 넘어가도록 하자.

# 1-B. 함수정의
---

## *function*
JS에서 `function`은 C/C++이나 Java와는 조금 다르다.

함수라는 특징(입력 파라미터와 리턴값이 존재)은 같으나 JS의 `function`은 객체로서 존재한다는 것이다.

---

## *function* 키워드로 정의하기
가장 일반적인 방법이다.

```
function foo()	{...}
```
---

## *var*(, *let*, *const*) 키워드로 정의하기
자주 사용하는 방법은 아니지만 아래의 형식으로도 정의할 수 있다.

```
var foo = function foo()	{...}

let foo = function foo()	{...}

const foo = function foo()	{...}
```
---

## *Arrow function*으로 정의하기
ES6에서 추가된 새로운 정의 방법이다.

```
var foo = () => {...}
```

## *Object* 내부에서 정의하기
`Object` 내부에서 정의하는 방법은 세 가지가 있다.

```
var obj = {
	foo: function foo()	{...}
}

var obj = {
	foo: () => {...}
}

var obj = {
	foo()	{...}
}
```

## *class* 내부 에서 정의하기
ES6에서 추가된 `class` 키워드 내에서 정의하는 방법이다.

```
class Car	{
	constructor()	{
		...
	}

	move()	{
		...
	}
}
```
---

## 익명함수(anonymous function)에 대하여
익명함수의 사용은 되도록 *Arrow Function* 으로 정의를 하는 것을 권장하고 있다.
자세한 이유는 *Arrow Function*에서 설명